using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Orraphan.GameDev3.Chapter2
{
    public class ArrowKeys  : MonoBehaviour
    {
        public float m_MovementStep;

        void Start()
        {
        
        }

        void Update()
        {
            if(Input.GetKey(KeyCode.LeftArrow))
            {
                this.transform.Translate(-m_MovementStep, 0, 0);
            }
            else if (Input.GetKey(KeyCode.RightArrow)) 
            {
                this.transform.Translate(m_MovementStep , 0, 0);
            }
            else if (Input.GetKey(KeyCode.UpArrow))
            {
                this.transform.Translate(0, m_MovementStep , 0);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                this.transform.Translate(0, -m_MovementStep, 0);
            }
        }
    }
}

